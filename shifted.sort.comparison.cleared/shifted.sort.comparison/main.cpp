#include <cstdio>
#include <vector>
#include <cstdlib>
#include <float.h>
#include <sys/time.h>
#include <ANN/ANN.h>

#include "KDtree.h"
#include "CUDA_KDtree.h"
#include "cubShiftedSort.cuh"

//#define _TEST_MODE
double TimeDiff(timeval t1, timeval t2);
double SearchCPU(const vector <Point> &query, const vector <Point> &data, vector <int> &idxs, vector <float> dist_sq);
void SearchANN(const vector <Point> &query, const vector <Point> &data, vector <int> &idxs, vector <float> dist_sq, double &create_time, double &search_time);


bool isUnit(float x)
{
	if( x >= 0.0f && x <= 1.0f)
		return true;
	else
		return false;
}

bool isInUnitCube(Point p)
{
	if( isUnit(p.coords[0]) && isUnit(p.coords[1]) && isUnit(p.coords[2]))
		return true;
	else
		return false;
}

void cudaDummyRun(void)
{
}

int main()
{
	cudaDeviceReset();

	selectTheBestGPU();



	
	KDtree tree;
    CUDA_KDTree GPU_tree;
    timeval t1, t2;
    int max_tree_levels = 13; // play around with this value to get the best result
	vector <Point> data(1 << 18);  vector <Point> queries(1 << 18); // maximum when k = 1 
	//vector <Point> data(1 << 22-1);  vector <Point> queries(1 << 22-1); // maximum when k = 1 
    
	vector <int> gpu_indexes, cpu_indexes;
    vector <float> gpu_dists, cpu_dists;
	FILE * fp;
    for(unsigned int i=0; i < data.size(); i++) {

        data[i].coords[0] = 0 + (rand() / (1.0 + RAND_MAX))*0.74;
        data[i].coords[1] = 0 + (rand() / (1.0 + RAND_MAX))*0.74;
        data[i].coords[2] = 0 + (rand() / (1.0 + RAND_MAX))*0.74;
	if(!isInUnitCube(data[i]))
		printf("\n*** data is not in unit cube!! ***\n");
    }

    for(unsigned int i=0; i < queries.size(); i++) {
//        queries[i].coords[0] = 0 + 100.0*(rand() / (1.0 + RAND_MAX));
//        queries[i].coords[1] = 0 + 100.0*(rand() / (1.0 + RAND_MAX));
//        queries[i].coords[2] = 0 + 100.0*(rand() / (1.0 + RAND_MAX));
        queries[i].coords[0] = 0 + (rand() / (1.0 + RAND_MAX))*0.74;
        queries[i].coords[1] = 0 + (rand() / (1.0 + RAND_MAX))*0.74;
        queries[i].coords[2] = 0 + (rand() / (1.0 + RAND_MAX))*0.74;
    }

    // Time to create the tree
  

	
	

	printf("\n Running gpu kd tree...");
	gettimeofday(&t1, NULL);
	tree.Create(data, max_tree_levels);
	GPU_tree.CreateKDTree(tree.GetRoot(), tree.GetNumNodes(), data);
    gettimeofday(&t2, NULL);
    double gpu_KD_tree_create_time = TimeDiff(t1,t2);
	
    // Time to search the tree
    gettimeofday(&t1, NULL);

	GPU_tree.Search(queries, gpu_indexes, gpu_dists);
    gettimeofday(&t2, NULL);
    double gpu_KD_tree_search_time = TimeDiff(t1,t2);
	
    printf("GPU kd tree create + search: %g + %g = %g ms\n", gpu_KD_tree_create_time, gpu_KD_tree_search_time, gpu_KD_tree_create_time + gpu_KD_tree_search_time);


	
	
	
    double ANN_create_time;
    double ANN_search_time;
    SearchANN(queries, data, cpu_indexes, cpu_dists, ANN_create_time, ANN_search_time);

    printf("\nANN create + search: %g + %g = %g ms\n", ANN_create_time, ANN_search_time, ANN_create_time +ANN_search_time);


	 printf("\n Length of data : %lu", data.size());
	 printf("\n Length of queries : %lu", queries.size());
	
	 printf("\n");



	// Time to create the shifted sort
	
	printf("\n Running dummy kernel...");
	//cudaDeviceReset();
	
	double gpu_shifted_create_time = 0.0;
	// Time to search the shifted sort
	gettimeofday(&t1, NULL);
	unsigned int * pIndices = new unsigned int[queries.size()*NUM_NEIGH];
	float * pDistances = new float[queries.size()*NUM_NEIGH];

	kANNShiftedSortNoCache(data, queries,pIndices, pDistances, data.size(), queries.size());
	cudaPeekAtLastError();

	gettimeofday(&t2, NULL);
	double gpu_shifted_search_time = TimeDiff(t1,t2);
    printf("shifted create + shifted sort search:  %g ms\n", gpu_shifted_create_time+gpu_shifted_search_time);

		
	fp = fopen("result_shifted.log", "w");
	fprintf(fp, "----------------------------------------------------------------------------------------\n");		
	fprintf(fp,"|\t qID \t|\t queries \t|\t  shifted gpu indices \t|\t  shifted gpu distances \t|\n");
    for(unsigned int i=0; i < NUM_NEIGH*queries.size(); i++)
	{
		unsigned int foundGPUShiftedIdx = pIndices[i];

		float gpu_shifted_dist = pDistances[i];
		unsigned int qID = i/NUM_NEIGH;
		if(foundGPUShiftedIdx  == 1)
			fprintf(fp,">>");

		if(i % NUM_NEIGH == 0)
				fprintf(fp, "----------------------------------------------------------------------------------------\n");		
        fprintf(fp,"\t%u\t|\t(%f, %f, %f)\t|\t %u\t|\t  %f\t| \n", qID, queries[qID].coords[0], queries[qID].coords[1], queries[qID].coords[2], foundGPUShiftedIdx,	 gpu_shifted_dist );
	

  	}
	fclose(fp);
	/*
	fp = fopen("cpu vs shifted.txt", "w");
	fprintf(fp,"------------------------------------------\n");
	fprintf(fp,"|\t qID \t|\t queries \t|\t cpu indices \t|\t  shifted gpu indices \t|\t cpu distances  \t|\t  shifted gpu distances \t|\n");
	fprintf(fp,"------------------------------------------\n");
    for(unsigned int i=0; i < queries.size(); i++)
	{
		
		int foundCPUIdx = cpu_indexes[i];
		int foundGPUShiftedIdx = pIndices[i];

		float cpu_dist = distance(&queries[i], &data[foundCPUIdx]);
//		float gpu_shifted_dist = distance(&queries[i], &data[foundGPUShiftedIdx]);
		float gpu_shifted_dist = pDistances[i];

        fprintf(fp,"\t%u\t|\t(%f, %f, %f)\t|\t %d\t|\t  %d\t|\t %f \t|\t %f  \n", i, queries[i].coords[0], queries[i].coords[1], queries[i].coords[2], foundCPUIdx,foundGPUShiftedIdx,	cpu_dist, gpu_shifted_dist );
  	}
	fclose(fp);
*/

	fp = fopen("cpu_indexes.txt", "w");
   	for(unsigned int i=0; i < cpu_indexes.size(); i++) 
	{		
		fprintf(fp,"%d\n", cpu_indexes[i]);
	}
	fclose(fp);	
	
   
    // Verify results
    for(unsigned int i=0; i< gpu_indexes.size(); i++) {
        if(gpu_indexes[i] != cpu_indexes[i]) {
            printf("Resuts not the same :(\n");
			printf("i = %d\n",i);
            printf("%d != %d\n", gpu_indexes[i], cpu_indexes[i]);
            printf("%f %f\n", gpu_dists[i], cpu_dists[i]);
            return 1;
        }
    }
	
    printf("Points in the tree: %ld\n", data.size());
    printf("Query points: %ld\n", queries.size());
    printf("\n");

    printf("Results are the same!\n");

    printf("\n");
	  	
    printf("GPU max kd tree depth: %d\n", max_tree_levels);
    printf("GPU kd tree create + search: %g + %g = %g ms\n", gpu_KD_tree_create_time, gpu_KD_tree_search_time, gpu_KD_tree_create_time + gpu_KD_tree_search_time);
    printf("ANN create + search: %g + %g = %g ms\n", ANN_create_time, ANN_search_time, ANN_create_time +ANN_search_time);
    printf("shifted create + shifted sort search: %g + %g = %g ms\n",  gpu_shifted_create_time,gpu_shifted_search_time, gpu_shifted_create_time +gpu_shifted_search_time);
    printf("Speed up of GPU over CPU for searches: %.2fx\n", ANN_search_time / gpu_KD_tree_search_time);
	

    return 0;
}

double TimeDiff(timeval t1, timeval t2)
{
    double t;
    t = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    t += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms

    return t;
}

double SearchCPU(const vector <Point> &queries, const vector <Point> &data, vector <int> &idxs, vector <float> dist_sq)
{
    timeval t1, t2;

    int query_pts = queries.size();
    int data_pts = data.size();

    idxs.resize(query_pts);
    dist_sq.resize(query_pts);

    gettimeofday(&t1, NULL);
    for(unsigned int i=0; i < query_pts; i++) 
	{
        float best_dist = FLT_MAX;
        int best_idx = 0;

        for(unsigned int j=0; j < data_pts; j++) 
		{
            float dist_sq = 0;

            for(int k=0; k < KDTREE_DIM; k++) {
                float d = queries[i].coords[k] - data[j].coords[k];
                dist_sq += d*d;
            }

            if(dist_sq < best_dist) {
                best_dist = dist_sq;
                best_idx = j;
            }
        }

        idxs[i] = best_idx;
        dist_sq[i] = best_dist;
    }

    gettimeofday(&t2, NULL);

    return TimeDiff(t1,t2);
}

void SearchANN(const vector <Point> &queries, const vector <Point> &data, vector <int> &idxs, vector <float> dist_sq, double &create_time, double &search_time)
{
    int k = 1;
    timeval t1, t2;

    idxs.resize(queries.size());
    dist_sq.resize(queries.size());

    ANNidxArray nnIdx = new ANNidx[k];
    ANNdistArray dists = new ANNdist[k];
    ANNpoint queryPt = annAllocPt(KDTREE_DIM);

    ANNpointArray dataPts = annAllocPts(data.size(), KDTREE_DIM);

    for(unsigned int i=0; i < data.size(); i++) {
        for(int j=0; j < KDTREE_DIM; j++ ) {
            dataPts[i][j] = data[i].coords[j];
        }
    }

    gettimeofday(&t1, NULL);
    ANNkd_tree* kdTree = new ANNkd_tree(dataPts, data.size(), KDTREE_DIM);
    gettimeofday(&t2, NULL);
    create_time = TimeDiff(t1,t2);

    gettimeofday(&t1, NULL);
    for(int i=0; i < queries.size(); i++) 
	{
        for(int j=0; j < KDTREE_DIM; j++) 
		{
            queryPt[j] = queries[i].coords[j];
        }

        kdTree->annkSearch(queryPt, 1, nnIdx, dists);

        idxs[i] = nnIdx[0];
        dist_sq[i] = dists[0];
    }
    gettimeofday(&t2, NULL);
    search_time = TimeDiff(t1,t2);

	delete [] nnIdx;
	delete [] dists;
	delete kdTree;
	annDeallocPts(dataPts);
	annClose();
}


﻿/**
 * @file   cubShiftedSort.cuh
 * @Author Taejung Park (taejung.park at gmail.com)
 * @date   March 6 2016
 * @brief  CUDA kernel for shifted sort.
 *
 *CUDA kernel for shifted sort.
 */

#define CUB_STDERR

#include "cubShiftedSort.cuh"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cub/util_allocator.cuh>
#include <cub/device/device_radix_sort.cuh>
#include <cub/device/device_scan.cuh>
#include <cub/iterator/transform_input_iterator.cuh>  
#include <cub/device/device_select.cuh>
#include <cub/device/device_partition.cuh>
#include <cub/iterator/counting_input_iterator.cuh>
#include <cub/warp/warp_scan.cuh>

#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sort.h>

#include <device_functions.h>
#include <string>
#include <sstream>
#include "cudaWatch.cuh"
#include "cudaUtils.cuh"
#include "commonTJ.h"
#include "tjvector_types.h"


//#define __TEST_BLOCK__
#define __USE_THRUST__
#define	EUCLIDEAN_DIST_BASED
//#define	USING_FINAL_SORT

void selectTheBestGPU(void)
{
		// select the best device
	int numDevices = 0;
	cudaDeviceProp props;
	cudaDeviceProp bestProps;

	int maxMultiprocessors = 0, maxDevice = 0;
	int device;
	cudaGetDeviceCount(&numDevices);
	printf("\n List of GPUs...");
	if (numDevices > 1) 
	{
		for (device=0; device<numDevices; device++) 
		{
			
			cudaGetDeviceProperties(&props, device);
			printf("\n - Device %d: %s, # of multiprocesseros = %d", device, props.name, props.multiProcessorCount);
			if (maxMultiprocessors < props.multiProcessorCount) 
			{
				bestProps = props;
				maxMultiprocessors = props.multiProcessorCount;
				maxDevice = device;
			}
		}
		cudaSetDevice(maxDevice);
		printf("\nUsing Device %d: %s", maxDevice, bestProps.name);
		printf("\nTotal Global memory %llu (MB)", bestProps.totalGlobalMem / (1<<20));
	}
}
__host__ __device__ float squaredEuclideanDistance(float x1, float y1, float z1, float x2, float y2, float z2)
{
	return (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2);
}


void writeInBase2(FILE * fp, unsigned long long a)
{
	unsigned long long sift = 1LL << 63;
	unsigned int v;
	while(sift!=0LL)
	{
		if(sift & a)
			v = 1;
		else 
			v = 0;
		fprintf(fp, "%u", v);
		sift = sift >> 1;
	}

}

float distance(Point * pA, Point * pB)
{
	float val = 0.0f;
	for(int i = 0 ; i < KDTREE_DIM ; i++)
	{
		float a = pA->coords[i] - pB->coords[i];
		val += a*a;
	}
	return sqrt(val);
}

	
	
cub::CachingDeviceAllocator g_allocator(true);

__device__ int warp_scan(int val, volatile int * s_data)
{
	int idx = 2* threadIdx.x - (threadIdx.x * (warpSize -1 ));
	s_data[idx] = 0;
	idx += warpSize;
	int t = s_data[idx] = val;
	s_data[idx] = t = t + s_data[idx - 1];
	s_data[idx] = t = t + s_data[idx - 2];
	s_data[idx] = t = t + s_data[idx - 4];
	s_data[idx] = t = t + s_data[idx - 8];
	s_data[idx] = t = t + s_data[idx -16];
	return s_data[idx-1];
}


__device__ unsigned int lanemask_lt()
{
 /*
	unsigned int mask;
	asm("mov.u32 %0, %lanemask lt;" : "=r"(mask));
	return mask;
	*/
	
	const unsigned int lane = threadIdx.x & (WARP_SIZE -1);
	return (1 << (lane)) - 1;
	
}
__device__ unsigned int lanemask_le()
{
 /*
	unsigned int mask;
	asm("mov.u32 %0, %lanemask lt;" : "=r"(mask));
	return mask;
	*/
	
	const unsigned int lane = threadIdx.x & (WARP_SIZE -1);
	return (1 << (lane+1)) - 1;
	
}


/**
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  
*@brief sortBasedOnDistance 
*@details improved from "Optimizing Parallel Prefix Operations for the Fermi Architecture" Wen-mei W. Hwu (Eds.) GPU Computing Gems Jade Edition  2011, pp 40-
*@param
* 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
__device__ unsigned int warp_segscan(bool p, unsigned int hd)
{
	const unsigned int masklt = lanemask_lt();
	const unsigned int maskle = lanemask_le();
	hd = (hd | 1) & maskle;
	unsigned int above = __clz(hd) + 1;

	unsigned int segmask = ~((~0U) >> above);
// Perform the scan
	unsigned int b = __ballot(p);
	return __popc(b & masklt & segmask);
}

__device__ unsigned int warp_prefix_sums(bool p)
{
	const unsigned int mask = lanemask_lt();
	unsigned int b = __ballot(p);
	return __popc(b & mask);
}

__device__ int block_binary_prefix_sums(int x)
{
	extern __shared__ int sdata[];
	// A. Compute exclusive prefix sums within each warp
	int warpPrefix = warp_prefix_sums(x);
	int idx = threadIdx.x;
	int warpIdx = idx / warpSize;
	int laneIdx = idx & (warpSize - 1);
	// B. The last thread of each warp stores inclusive
	// prefix sum to the warp’s index in shared memory
	if (laneIdx == warpSize - 1)
		sdata[warpIdx] = warpPrefix + x;
	__syncthreads();
	// C. One warp scans the warp partial sums
	if (idx < warpSize)
		sdata[idx] = warp_scan(sdata[idx], sdata);
	__syncthreads();
	// D. Each thread adds prefix sums of warp partial
	// sums to its own intra􀀀warp prefix sums
	return warpPrefix + sdata[warpIdx];
}

struct IsDataPoint
{
	unsigned int m_numDatapoints;

	IsDataPoint(unsigned int v)	{ m_numDatapoints =v;};

	template <typename T>
	__host__ __device__ __forceinline__
		unsigned int operator()(const T &a) const 
	{		
		if(a < m_numDatapoints) 
			return 1u;
		else
			return 0u;
	}
};

struct ui3vec
{
	unsigned int v[3];	
};


__host__ __device__ ui3vec convertULL2UI3Vec(unsigned long long a)
{
	unsigned long long ULLsift = 1;
	unsigned int Usift = 1;
	unsigned int count = 0;
	unsigned int idx = 0;
	ui3vec result;
	int i;
	for(i = 0 ; i < 3; i++)
		result.v[i] = 0u;


	do
	{
		idx = count % 3;
		if(ULLsift & a)
			result.v[idx] |= Usift;
		if(idx == 2)
			Usift <<= 1;

		ULLsift <<= 1;
		count++;
	}while(ULLsift != 0x00);



	return result;

}
__host__ __device__ unsigned int getIntraWarpFilter(unsigned int k)
{
	unsigned int result = 0;
	unsigned int sift = 1;
	unsigned int iter = WARP_SIZE/k;

	unsigned int i;

	for(i=0; i< iter; i++)
	{
		sift <<= k;
		result |= sift;
	}
	return result;
}
__host__ __device__ float getFloatSqrDistance(unsigned long long a, unsigned long long b)
{
	int i;
	ui3vec aui, bui;
	float fa;
	float result = 0.0f;
	aui = convertULL2UI3Vec(a);
	bui = convertULL2UI3Vec(b);

	for(i = 0 ; i < 3; i++)
	{
		fa = ( ((float)aui.v[i])/2097152.0f - ((float)bui.v[i])/2097152.0f);
		result += fa*fa;
	}

	return result;
}

__host__ __device__ unsigned int getTruncatedUIntSqrDistance(unsigned long long a, unsigned long long b, unsigned int numBits)
{
	int i;
	ui3vec aui, bui;
	unsigned int uia;
	unsigned int result = 0u;
	aui = convertULL2UI3Vec(a);
	bui = convertULL2UI3Vec(b);
	unsigned int sift = 21 - numBits;	// 21 == 64/3
	for(i = 0 ; i < 3; i++)
	{
		uia = (aui.v[i] >> sift) - (bui.v[i] >> sift) ;
		result += uia*uia;
	}

	return result;
}
__host__ __device__ unsigned int getUIntSqrDistance(unsigned long long a, unsigned long long b)
{
	int i;
	ui3vec aui, bui;
	unsigned int uia;
	unsigned int result = 0u;
	aui = convertULL2UI3Vec(a);
	bui = convertULL2UI3Vec(b);

	for(i = 0 ; i < 3; i++)
	{
		uia = (aui.v[i]- bui.v[i]);
		result += uia*uia;
	}

	return result;
}
__global__	void initializeIndex(unsigned int * d_index, unsigned int num)
{

	const unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;

	if(num <= tid)
		return;
	d_index[tid] = tid;
}
/**
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  
*@brief extractNearest 
*@details extracts the nearest indices
*@param
* 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
__global__	void extractNearest(
	unsigned int * d_AllNNIdx, 
	unsigned int * d_outNearestIdx,	// size == num of query points
	unsigned int numQueryPoints, 
	unsigned int numNeighbors)
{
	const unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
	if(numNeighbors*numQueryPoints <= tid)	  
		return;
	unsigned int pos = tid/numNeighbors;
	if(tid % numNeighbors == 0)
		d_outNearestIdx[pos] = d_AllNNIdx[tid];
}



/**
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  
*@brief finalSort 
*@details finally sorts the k-nearest indices per query
*@param
* 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
__global__	void finalSort(
	unsigned int * d_AllNNIdx, 
	unsigned int * d_outFinalIdx,
	unsigned int * d_outSortedAllNNIdx,
	float * d_AllNNdist,
	unsigned int hd,
	unsigned int numQueryPoints, 
	unsigned int numNeighbors)
{

	const unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;

	if(numNeighbors*numQueryPoints <= tid)	  // # of total threads should be "2*numNeighbors*numQueryPoints"
		return;
	
	__shared__ float dist[WARP_SIZE];
	__shared__ unsigned int dataindices[WARP_SIZE];

	unsigned int laneIdx = tid & ( WARP_SIZE -1);
	unsigned int index;
	float fdist;

	dataindices[laneIdx] = d_AllNNIdx[tid];
	dist[laneIdx] = d_AllNNdist[tid];

	__syncthreads();	
	
	bool b, e;
	unsigned int sift, f, t, d, totalFalses, groupFirstIdx, groupLastIdx;
	groupFirstIdx  = (laneIdx/(numNeighbors))* numNeighbors; 
	groupLastIdx = (1u + laneIdx/(numNeighbors))* numNeighbors - 1u; 

	for(sift =1u; sift != 0u ; sift <<= 1)
	{
		fdist = dist[laneIdx];
		index = dataindices[laneIdx];
	 	b = sift &(* (unsigned int *) &fdist);
		e = !b;		   
		f = warp_segscan(e,hd);		

		totalFalses =  __shfl(e,groupLastIdx) +  __shfl(f,groupLastIdx);	//	x = __shfl(x, 0); // All the threads read x from laneid 0.

		t = laneIdx - f + totalFalses;
		d = b ? t: f+groupFirstIdx;


		dataindices[d]	=	index;
		dist[d]			=	fdist;
	
		__syncthreads();

	}	

	d_outSortedAllNNIdx[tid] = dataindices[laneIdx];
	if(laneIdx  ==  groupFirstIdx)
	{// store back to the gmem
		unsigned int indexk = tid / numNeighbors;
		
		d_outFinalIdx[indexk] = dataindices[laneIdx];	

	}
	
	
}


/**
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  
*@brief sortBasedOnDistance 
*@details
*@param
* in : //  d_2kNNIdx			: size == 2k*(num queries)
* d_2kNNIdx			: size == 2k*(num queries)
*  d_locally_sorted_MC		: size == (num queries) + (num data)
*  d_partitioned_MC		: size == (num queries) + (num data)
*      numDataPoints,  numQueryPoints, numNeighbors
* out :
*  d_AllNNIdx : uses only k*(num queries) -> allNNIdx[ ]
*  d_AllNNDist : uses only k*(num queries) -> allNNDist[ ]
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
__global__	void sortBasedOnDistance(
	unsigned int * d_AllNNIdx, 
	unsigned int * d_AllNNDist,
	unsigned int * d_2kNNIdx, 
	unsigned int * d_2kNNDist,
	unsigned long long * d_locally_sorted_MC, 
	unsigned long long * d_partitioned_MC, 
	unsigned int hd,
	unsigned int numDataPoints, 
	unsigned int numQueryPoints, 
	unsigned int numNeighbors)
{
 	const unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
	if(2*numNeighbors*numQueryPoints <= tid)	  // # of total threads should be "2*numNeighbors*numQueryPoints"
		return;
	__shared__ unsigned long long datapointMC[WARP_SIZE];
	__shared__ unsigned int uiDistances[WARP_SIZE];
	__shared__ unsigned int dataindices[WARP_SIZE];

	
	unsigned int queryID = tid/(2*numNeighbors);
	unsigned int uiSqrDist, dataIdx; 
	unsigned long long	dataMC, queryMC;
	

 	int laneIdx = tid & ( WARP_SIZE -1);
//	int warpIdx = tid / WARP_SIZE;


	// copy query MCs from the gmem to the smem.

	queryMC		= d_partitioned_MC[queryID+numDataPoints];	
	dataIdx =	dataindices[laneIdx]		= d_2kNNIdx[tid];
	dataMC =	datapointMC[laneIdx]		= d_locally_sorted_MC[tid];
	uiSqrDist					= getTruncatedUIntSqrDistance(datapointMC[laneIdx], queryMC, TRUNC_BIT_RESOLUTION);
	uiDistances[laneIdx]		= uiSqrDist;

//	//printf("\n thread %d, dist = %u", tid, uiDistances[laneIdx]);
  
  	__syncthreads();	


	bool b, e;
	unsigned int sift, f, t, d, totalFalses, groupID;
	groupID = (1u+laneIdx / (2u*numNeighbors)) * 2u*numNeighbors -1u;

	for(sift =1u; sift != 0u ; sift <<= 1)
	{

	 	b = sift & uiSqrDist;
		e = !b;		   
		f = warp_segscan(e,hd);
		totalFalses =  __shfl(e,groupID) +  __shfl(f,groupID);	//	x = __shfl(x, 0); // All the threads read x from laneid 0.
		t = laneIdx - f + totalFalses;
		d = b ? t: f+(groupID - (2u*numNeighbors -1u));
  		uiDistances[d] =	uiSqrDist;
		datapointMC[d] =	dataMC;
		dataindices[d] =	dataIdx;
	
		__syncthreads();

	}	


	if((laneIdx / numNeighbors)%2 == 0)
	{// store back to the gmem
		unsigned int index2k = tid / (2u*numNeighbors);
		unsigned int newID = tid - index2k * numNeighbors;
		
		d_AllNNIdx[newID] = dataindices[laneIdx];
		d_locally_sorted_MC[newID] = datapointMC[laneIdx];
		d_AllNNDist[newID] = uiDistances[laneIdx];
	}
	
	/*
	d_2kNNIdx[tid] = dataindices[laneIdx];
	d_locally_sorted_MC[tid] = datapointMC[laneIdx];
	testUIntDistance[tid] = uiDistances[laneIdx];
	*/
}


/**
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  
*@brief sortBasedOnEuclideanDistance 
*@details
*@param
* in : //  d_2kNNIdx			: size == 2k*(num queries)
* d_2kNNIdx			: size == 2k*(num queries)
*  d_out_locally_sorted_MC		: size == (num queries) + (num data)
*  d_partitioned_MC		: size == (num queries) + (num data)
*      numDataPoints,  numQueryPoints, numNeighbors
* out :
*  d_out_AllNNDist : uses only k*(num queries) -> allNNIdx[ ]
*  d_out_AllNNDist : uses only k*(num queries) -> allNNDist[ ]
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
__global__	void sortBasedOnEuclideanDistance(
	unsigned int * d_out_AllNNIdx, 
	float * d_out_AllNNDist,
	unsigned int * d_2kNNIdx, 
	unsigned int hd,
	float * px, float * py, float * pz, 
	unsigned int numDataPoints, 
	unsigned int numQueryPoints, 
	unsigned int numNeighbors)
{
 	const unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
	if(2*numNeighbors*numQueryPoints <= tid)	  // # of total threads should be "2*numNeighbors*numQueryPoints"
		return;

	extern __shared__ float shared_pool[];
	float *					fDistances	= (float *) (shared_pool);
	unsigned int *			dataindices	= (unsigned int *) (shared_pool + blockDim.x);

	float fSqrDist;
	unsigned int queryID = tid/(2*numNeighbors);
	unsigned int dataIdx; 
	unsigned long long	dataMC; //, queryMC;
	
	float qx = px[queryID+numDataPoints];
	float qy = py[queryID+numDataPoints];
	float qz = pz[queryID+numDataPoints];
	float dx,dy,dz;

	unsigned int laneIdx = tid & ( WARP_SIZE -1);
	unsigned int warpIdx = threadIdx.x / WARP_SIZE;
	
	// copy query MCs from the gmem to the smem.


	
	dataindices[threadIdx.x]		= d_2kNNIdx[tid];

	dataIdx							= dataindices[threadIdx.x];

	dx = px[dataIdx];
	dy = py[dataIdx];
	dz = pz[dataIdx];
	
	fSqrDist						= squaredEuclideanDistance(qx, qy, qz, dx, dy, dz);
	fDistances[threadIdx.x]			= fSqrDist;
	
//	//printf("\n thread %d, dist = %u", tid, uiDistances[laneIdx]);
  
  	__syncthreads();	
/**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>
*@details GPU Gems 3 Chapter 39. Parallel Prefix Sum (Scan) with CUDA
*        groupFirstIdx = ( laneIdx / (2*k) ) * 2*k
*        groupLastIdx = groupFirstIdx + (group length) = ( laneIdx / (2*k) ) * 2*k + 2*k  - 1
*                     = ( 1 + laneIdx / (2*k) ) * 2*k - 1
*@param
* 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
	

	bool b, e;
	unsigned int sift, f, t, d, totalFalses, groupFirstIdx, groupLastIdx, blockWideIndex;

	groupFirstIdx  = (laneIdx/(2u*numNeighbors))* 2u*numNeighbors; 
	groupLastIdx = (1u + laneIdx/(2u*numNeighbors))* 2u*numNeighbors - 1u; 
	blockWideIndex = 0;
	
#pragma unroll
	for(sift =1u; sift != 0u ; sift <<= 1)
	{
		dataIdx		= dataindices[threadIdx.x];
		fSqrDist	= fDistances[threadIdx.x];
	
	 	b =  sift &(* (unsigned int *) &fSqrDist);
		e = !b;		   
		f = warp_segscan(e,hd);
		totalFalses =  __shfl(e,groupLastIdx) +  __shfl(f,groupLastIdx);	//	x = __shfl(x, 0); // All the threads read x from laneid 0.
		t = laneIdx - f + totalFalses;
		d = b ? t: f+groupFirstIdx;

		blockWideIndex = d + warpIdx * WARP_SIZE;

  		fDistances[blockWideIndex] =	fSqrDist;
		dataindices[blockWideIndex] =	dataIdx;

		__syncthreads();

	}	
	

	if((laneIdx / numNeighbors)%2 == 0)
	{// store back to the gmem
		unsigned int index2k = tid / (2u*numNeighbors); 
		unsigned int newID = tid - index2k * numNeighbors;//+ warpIdx * WARP_SIZE; 
		
		d_out_AllNNIdx[newID] = dataindices[threadIdx.x];
		d_out_AllNNDist[newID] = fDistances[threadIdx.x];

	}


}




/**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
*@brief storeNNCandidates 
*@details
*@param
* in : 
*  d_partitionedMixedIndices	: size == (num queries) + (num data)
*  d_partitioneExSum			: size == (num queries) + (num data)
*  d_partitioned_MC			: size == (num queries) + (num data)
*      numDataPoints,  numQueryPoints, numNeighbors
* out :
*  d_2kNNIdx
*  d_2kNNDist
*  d_locally_sorted_MC
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/


__global__	void storeNNCandidates(
	unsigned int * d_partitionedMixedIndices, 
	unsigned int * d_partitioneExSum, 
	unsigned long long * d_partitioned_MC, 
	unsigned int * d_2kNNIdx,  
	unsigned int * d_2kNNDist, 
	unsigned long long * d_locally_sorted_MC, 
	unsigned int numDataPoints, 
	unsigned int numQueryPoints, 
	unsigned int numNeighbors)
{
	const unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
	if(numQueryPoints <= tid)
		return;
	unsigned int queryID = d_partitionedMixedIndices[tid+numDataPoints]-numDataPoints;
	unsigned int nearest_exsum_index = d_partitioneExSum[tid+numDataPoints];
	unsigned long long queryMC = d_partitioned_MC[queryID+numDataPoints]; // check.

	if(nearest_exsum_index > numDataPoints)	// means error!
	{
		//printf("\n Error [device] organizeNNCandidates - tid : %u, nearest_exsum_index = %u, numDataPoints = %u.\n", tid, nearest_exsum_index, numDataPoints, numDataPoints);
		return;
	}
	if(nearest_exsum_index == numDataPoints)
		nearest_exsum_index --;


	int i, start, end, offset, count;
	offset = 0;
	count = 0;
	start = nearest_exsum_index - numNeighbors -1;
	end = nearest_exsum_index + numNeighbors;
	if(	start < 0)
		offset = numNeighbors - nearest_exsum_index;
	else if(end > numDataPoints)
		offset = numDataPoints - nearest_exsum_index - numNeighbors - 1;
	start += offset;
	end += offset;
	unsigned long long datapointMC;

	for( i = start+1 ; i < end; i++)
	{
		d_2kNNIdx[2*numNeighbors*queryID + count] = d_partitionedMixedIndices[i];	
		datapointMC = d_partitioned_MC[i];	// MCs for data points 
		d_locally_sorted_MC[2*numNeighbors*queryID + count] = datapointMC;	// MCs for data points 
		d_2kNNDist[2*numNeighbors*queryID + count] = getTruncatedUIntSqrDistance(datapointMC, queryMC, TRUNC_BIT_RESOLUTION);
		count++;

	}

}

/**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
*@brief storeNNCandidatesEuclidean 
*@details
*@param
* in : 
*  d_partitionedMixedIndices	: size == (num queries) + (num data)
*  d_partitioneExSum			: size == (num queries) + (num data)
*  d_partitioned_MC			: size == (num queries) + (num data)
*      numDataPoints,  numQueryPoints, numNeighbors
* out :
*  d_2kNNIdx
*  d_2kNNDist
*  d_locally_sorted_MC
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/



__global__ void storeNNCandidatesEuclidean(
	unsigned int * d_partitionedMixedIndices, 
	unsigned int * d_partitioneExSum, 
	unsigned long long * d_partitioned_MC, 
	unsigned int * d_2kNNIdx,  
	float * d_2kNNDist, 
	unsigned long long * d_locally_sorted_MC, 
	float * px, float * py, float * pz,
	unsigned int numDataPoints, 
	unsigned int numQueryPoints, 
	unsigned int numNeighbors)
{
	const unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
	if(numQueryPoints <= tid)
		return;
	unsigned int queryID = d_partitionedMixedIndices[tid+numDataPoints]-numDataPoints;
	unsigned int nearest_exsum_index = d_partitioneExSum[tid+numDataPoints];
	unsigned long long queryMC = d_partitioned_MC[queryID+numDataPoints]; // check.
	float qx = px[queryID+numDataPoints];
	float qy = py[queryID+numDataPoints];
	float qz = pz[queryID+numDataPoints];
	float dx,dy,dz;

	if(nearest_exsum_index > numDataPoints)	// means error!
	{
		//printf("\n Error [device] organizeNNCandidates - tid : %u, nearest_exsum_index = %u, numDataPoints = %u.\n", tid, nearest_exsum_index, numDataPoints, numDataPoints);
		return;
	}
	if(nearest_exsum_index == numDataPoints)
		nearest_exsum_index --;


	int i, start, end, offset, count;
	offset = 0;
	count = 0;
	start = nearest_exsum_index - numNeighbors -1;
	end = nearest_exsum_index + numNeighbors;
	if(	start < 0)
		offset = numNeighbors - nearest_exsum_index;
	else if(end > numDataPoints)
		offset = numDataPoints - nearest_exsum_index - numNeighbors - 1;
	start += offset;
	end += offset;
	unsigned long long datapointMC;

	for( i = start+1 ; i < end; i++)
	{
		unsigned dataPointID = d_partitionedMixedIndices[i];
		dx = px[dataPointID];
		dy = py[dataPointID];
		dz = pz[dataPointID];

		d_2kNNIdx[2*numNeighbors*queryID + count] = dataPointID;	
		datapointMC = d_partitioned_MC[i];	// MCs for data points 
		d_locally_sorted_MC[2*numNeighbors*queryID + count] = datapointMC;	// MCs for data points 
		d_2kNNDist[2*numNeighbors*queryID + count] = squaredEuclideanDistance(qx, qy, qz, dx, dy, dz);
		count++;

	}

}


__global__	void flagDataPoints(unsigned int * d_pMixedIndices, unsigned int * d_pOut_flags, unsigned int numDataPoints, unsigned int num)
{
	const unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
	if(num <= tid)
		return;
	if(d_pMixedIndices[tid] < numDataPoints)
		d_pOut_flags[tid] = 1u;
	else
		d_pOut_flags[tid] = 0u;
}


__global__	void computeMortoncodes(
	float *d_x, 
	float *d_y, 
	float *d_z, 
	unsigned long long * d_shiftedMorton, 
	float offset, 
	unsigned int numDataPoints, 
	unsigned int numQueryPoints)
{
	const unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
	if(numQueryPoints + numDataPoints <= tid)
		return;
	float x, y, z;
	x = d_x[tid];
	y = d_y[tid];
	z = d_z[tid];
	unsigned long long flag = (unsigned long long) (tid >= numDataPoints);

	d_shiftedMorton[tid] = (morton3D64(x+offset, y+offset, z+offset) << 1) | flag;


}

__global__ void testWarpScan(unsigned int * d_in,  unsigned int * d_out)
{
	unsigned int dataSize = 32u;

	const unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
	if(dataSize <= threadIdx.x )	  // # of total threads should be "numNeighbors*numQueryPoints"
		return;

	unsigned int warp_id = threadIdx.x / WARP_SIZE;
	typedef cub::WarpScan<unsigned int,4> WarpScan;

	__shared__ typename WarpScan::TempStorage temp_storage;	// # of warps
	__shared__ unsigned int sh_in[WARP_SIZE];
	__shared__ unsigned int sh_out[WARP_SIZE];
	sh_in[threadIdx.x] = d_in[tid];

	WarpScan(temp_storage).ExclusiveSum(sh_in[threadIdx.x], sh_out[threadIdx.x]);
	d_out[tid] = sh_out[threadIdx.x];
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// keep_K_NearestNeighborsDistOverWarp // distance version
// in : 
//  d_allNNIdx				: size == (num queries) * k
//  d_allNNDist				: size == (num queries) * k 
//  d_newlyArrivedNNIdx		: size == (num queries) * 2k
//  d_newlyArrivedNNDist	: size == (num queries) * 2k
//  d_queryID				: size == (num queries)
//  d_queryMC				: size == (num queries)
//  d_dataPointMC			: size == (num data) --> not necessary. All neighbor indices are for data points
//      numDataPoints,  numQueryPoints, numNeighbors
// out : 
//  updated d_allNNIdx, d_allNNMC
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__	void keep_K_NearestNeighborsEuclideanDist(
	unsigned int * d_allNNIdx,  
	float * d_allNNDist, 
	unsigned int * d_newlyArrived2kNNIdx,  
	float * d_newlyArrived2kNNDist, 	
	unsigned int numDataPoints, 
	unsigned int numQueryPoints, 
	unsigned int numNeighbors)
{
 	

	const unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
	if(2*numNeighbors*numQueryPoints <= tid)	  // # of total threads should be "numNeighbors*numQueryPoints"
		return;


	extern __shared__ unsigned int			counter_shared_pool[];
 	unsigned int	* 		counter					= (unsigned int*)	counter_shared_pool;
	unsigned int	* 		counter_scan			= (unsigned int*)	(counter_shared_pool + blockDim.x);
	unsigned int	*		uiUpdatedDataNNIdx		= (unsigned int*)	(counter_shared_pool + 2*blockDim.x);
	float			*		fUpdatedDataNNDist		= (float*)			(counter_shared_pool + 3*blockDim.x);
	unsigned int	*		uiCurrentNNIdx			= (unsigned int*)	(counter_shared_pool + 4*blockDim.x);			// only valid for 0 ~ WARP_SIZE/2 -1
	float			*		fCurrentNNDist			= (float*)			(counter_shared_pool + 5*blockDim.x);			// only valid for 0 ~ WARP_SIZE/2 -1
	
	unsigned int	*		uiCandidate2kNNIdx		= (unsigned int*)	(counter_shared_pool + 6*blockDim.x);
	float			*		fCandidate2kNNDist		= (float*)			(counter_shared_pool + 7*blockDim.x);

	unsigned int laneIdx = tid & ( WARP_SIZE -1);
	unsigned int warpIdx = tid / WARP_SIZE;
	unsigned int groupIdx = threadIdx.x / (2*numNeighbors);
	unsigned int intrawarp_offset = groupIdx * numNeighbors;
	unsigned int start_point_at_block = blockIdx.x * (blockDim.x/2) +intrawarp_offset;
	

	typedef cub::WarpScan<unsigned int,2*NUM_NEIGH> WarpScan;

	__shared__ typename WarpScan::TempStorage temp_storage;	// # of warps

	// copy allNN from global memory into currentNN in shared memory


	uiCurrentNNIdx[threadIdx.x]	= d_allNNIdx[blockIdx.x * (blockDim.x/2)  + threadIdx.x];					// only valid for 0 ~ WARP_SIZE/2 -1
	fCurrentNNDist[threadIdx.x]	= d_allNNDist[blockIdx.x * (blockDim.x/2)  + threadIdx.x];					// only valid for 0 ~ WARP_SIZE/2 -1


	unsigned int offset = 0;
	counter_scan[threadIdx.x] = 0;	
	counter[threadIdx.x] = (threadIdx.x % 2);

	uiCandidate2kNNIdx[threadIdx.x] = d_newlyArrived2kNNIdx[tid];
	fCandidate2kNNDist[threadIdx.x] = d_newlyArrived2kNNDist[tid];
	//////////////////////////////////////////////////////////
	__syncthreads();
	int t, loc;
	bool bValid = false;
	
	

	float candidateDist = fCandidate2kNNDist[threadIdx.x];

	for(t = numNeighbors/2, loc = numNeighbors/2 + intrawarp_offset; t > 1 ; t /= 2)
	{
		
		if( fCurrentNNDist[loc] < candidateDist)
			loc += t/2 ;
		else if( candidateDist < fCurrentNNDist[loc-1])
			loc -= t/2 ;
		else 
			break;	// means the candidate is duplicated
		
	}


	if( (fCurrentNNDist[loc-1] < candidateDist) && ( candidateDist < fCurrentNNDist[loc]))
		bValid = true;
	else if(candidateDist < fCurrentNNDist[intrawarp_offset])  
	{
		loc = intrawarp_offset;
		bValid = true;
	}
	
	if(bValid)
		offset =atomicAdd(&counter[2*loc], 1);

	__syncthreads();
	  
    WarpScan(temp_storage).ExclusiveSum(counter[threadIdx.x], counter_scan[threadIdx.x]);

	unsigned int index = counter_scan[2*loc] + offset + intrawarp_offset;
	if(bValid && (index < intrawarp_offset+numNeighbors))
	{
		uiUpdatedDataNNIdx[index] = uiCandidate2kNNIdx[threadIdx.x];
		fUpdatedDataNNDist[index] = fCandidate2kNNDist[threadIdx.x];		
	}
	
	index = counter_scan[threadIdx.x]+intrawarp_offset;
	if((threadIdx.x % 2 == 1) && (index < intrawarp_offset+numNeighbors))
	{
		uiUpdatedDataNNIdx[index] = uiCurrentNNIdx[(threadIdx.x/2)];
		fUpdatedDataNNDist[index] = fCurrentNNDist[(threadIdx.x/2)];		
	}
	

	__syncthreads();
	if(threadIdx.x < blockDim.x/2)
	{
		unsigned int gIndx = threadIdx.x + blockIdx.x * blockDim.x/2;

		d_allNNIdx[gIndx] = uiUpdatedDataNNIdx[threadIdx.x];
		d_allNNDist[gIndx] = fUpdatedDataNNDist[threadIdx.x];
	
	}
	
}



extern "C"
	void kANNShiftedSortNoCache(std::vector <Point> DataPoints, 
	std::vector <Point> QueryPoints, 
	unsigned int * outIndices,
	float * outDistance,
	size_t numDataPoints, 
	size_t numQueryPoints)
{

	size_t device_mem_size = 0;
	FILE * fp;
	float j = 0.0f;
	size_t i;

	float * px;
	float * py;
	float * pz;

	unsigned int sum_data_and_query = numDataPoints+numQueryPoints;
	// Allocate device arrays
	
	size_t numOfNeighbors = NUM_NEIGH;		// "k"
	unsigned int blocksize = G_BLOCKSIZE;

	size_t num_2kNN_items = 2*numOfNeighbors * numQueryPoints;
	size_t num_kNN_items = numOfNeighbors * numQueryPoints;

	unsigned int* d_2kNNDistances;
	unsigned int * d_2kNNIndices;
	unsigned int* d_AllNNDistances;
	unsigned int * d_AllNNIndices;	
	unsigned int* d_flags_for_data_points;
	unsigned int* d_data_point_indices;
	unsigned long long * d_2kNN_locally_sorted_mortoncodes;
	float * d_x;
	float * d_y;
	float * d_z;
	float * d_2kNNEuclideanDistances;
	float * d_AllNNEuclideanDistances;

	//printf("\n ********** alloc mem.... **");
	int *d_num_selected_out   = NULL;
	CubDebugExit(cudaMalloc((void**)&d_2kNN_locally_sorted_mortoncodes, sizeof(unsigned long long) * num_2kNN_items));
	CubDebugExit(cudaMalloc((void**)&d_num_selected_out, sizeof(int)));
	CubDebugExit(cudaMalloc((void**)&d_flags_for_data_points, sizeof(unsigned int) * sum_data_and_query));
	CubDebugExit(cudaMalloc((void**)&d_data_point_indices, sizeof(unsigned int) * numDataPoints));
	CubDebugExit(cudaMalloc((void**)&d_2kNNIndices, sizeof(unsigned int) * num_2kNN_items));
	CubDebugExit(cudaMalloc((void**)&d_2kNNDistances, sizeof(unsigned int) * num_2kNN_items));
	CubDebugExit(cudaMalloc((void**)&d_2kNNEuclideanDistances, sizeof(float) * num_2kNN_items));
	CubDebugExit(cudaMalloc((void**)&d_AllNNEuclideanDistances, sizeof(float) * num_2kNN_items));
	CubDebugExit(cudaMalloc((void**)&d_AllNNDistances, sizeof(unsigned int) * num_kNN_items));
	CubDebugExit(cudaMalloc((void**)&d_AllNNIndices, sizeof(unsigned int) * num_kNN_items));

	device_mem_size += sizeof(int) + sizeof(unsigned long long) * num_2kNN_items + sizeof(unsigned int) * sum_data_and_query + sizeof(unsigned int) * numDataPoints 
		+ sizeof(unsigned int) * (2* num_2kNN_items + 2*num_kNN_items) + 2*sizeof(float) * num_2kNN_items;

	CubDebugExit(cudaMalloc((void**)&d_x, sizeof(float) * sum_data_and_query));
	CubDebugExit(cudaMalloc((void**)&d_y, sizeof(float) * sum_data_and_query));
	CubDebugExit(cudaMalloc((void**)&d_z, sizeof(float) * sum_data_and_query));

	device_mem_size += 3*sizeof(float) * sum_data_and_query;

	unsigned long long * d_morton_keys_buf;
	unsigned long long * d_morton_keys_alt_buf;
	CubDebugExit(cudaMalloc((void**)&d_morton_keys_buf, sizeof(unsigned long long) * sum_data_and_query));
	CubDebugExit(cudaMalloc((void**)&d_morton_keys_alt_buf, sizeof(unsigned long long) * sum_data_and_query));
	cub::DoubleBuffer<unsigned long long>	dDB_mortoncodes_keys(d_morton_keys_buf, d_morton_keys_alt_buf);

	unsigned int * d_mixed_indices_buf;
	unsigned int * d_mixed_indices_alt_buf;
	CubDebugExit(cudaMalloc((void**)&d_mixed_indices_buf, sizeof(unsigned int) * sum_data_and_query));
	CubDebugExit(cudaMalloc((void**)&d_mixed_indices_alt_buf, sizeof(unsigned int) * sum_data_and_query));

	cub::DoubleBuffer<unsigned int>			dDB_mixedindices_values(d_mixed_indices_buf, d_mixed_indices_alt_buf);
	
	unsigned int * d_exclusivescanned_data_buf;
	unsigned int * d_exclusivescanned_data_alt_buf;
	CubDebugExit(cudaMalloc((void**)&d_exclusivescanned_data_buf, sizeof(unsigned int) * sum_data_and_query));
	CubDebugExit(cudaMalloc((void**)&d_exclusivescanned_data_alt_buf, sizeof(unsigned int) * sum_data_and_query));

	cub::DoubleBuffer<unsigned int>			dDB_exclusivescanned_for_data_points(d_exclusivescanned_data_buf, d_exclusivescanned_data_alt_buf);

	unsigned int * ptr_part_mixed_indices; 
	unsigned int * ptr_part_ex_sum; 
	unsigned long long * ptr_part_mixed_mc; 

	CubDebugExit(cudaMalloc((void**)&ptr_part_mixed_indices, sizeof(unsigned int) * sum_data_and_query));
	CubDebugExit(cudaMalloc((void**)&ptr_part_ex_sum, sizeof(unsigned int) * sum_data_and_query));
	CubDebugExit(cudaMalloc((void**)&ptr_part_mixed_mc, sizeof(unsigned long long) * sum_data_and_query));

	device_mem_size += 3*sizeof(unsigned long long) * sum_data_and_query + 6*sizeof(unsigned int)*sum_data_and_query;

	printf("\n kANNShiftedSort : total device mem size = %llu bytes (%llu MB)\n", device_mem_size, device_mem_size / ( 1<< 20) );

	
	//printf("\n ********** alloc mem.... done. sum_data_and_query = %u **",sum_data_and_query);
	// Allocate temporary storage
	size_t  temp_storage_bytes_for_sort			= 0;
	size_t  temp_storage_bytes_for_es			= 0;
	size_t  temp_storage_bytes_for_partition	= 0;
	size_t  temp_storage_bytes_for_partition2	= 0;
	size_t  temp_storage_bytes_for_partition3	= 0;
	size_t  temp_storage_bytes_for_partition4	= 0;

	void    *d_temp_storage_for_sort		 = NULL;
	void    *d_temp_storage_for_es			 = NULL;
	void    *d_temp_storage_for_partition    = NULL;
	void    *d_temp_storage_for_partition2   = NULL;
	void	*d_temp_storage_for_partition3	 = NULL;
	void	*d_temp_storage_for_partition4	 = NULL;

	

	px = new float[sum_data_and_query];
	py = new float[sum_data_and_query];
	pz = new float[sum_data_and_query];

	for(i=0;i<numDataPoints;i++)
	{
		px[i] = DataPoints[i].coords[0];
		py[i] = DataPoints[i].coords[1];
		pz[i] = DataPoints[i].coords[2];	
	}	
	for(i=0;i<numQueryPoints;i++)
	{
		px[i+numDataPoints] = QueryPoints[i].coords[0];
		py[i+numDataPoints] = QueryPoints[i].coords[1];
		pz[i+numDataPoints] = QueryPoints[i].coords[2];
	}

	cudaMemcpy(d_x, px, sizeof(float) * sum_data_and_query, cudaMemcpyHostToDevice);
	cudaMemcpy(d_y, py, sizeof(float) * sum_data_and_query, cudaMemcpyHostToDevice);
	cudaMemcpy(d_z, pz, sizeof(float) * sum_data_and_query, cudaMemcpyHostToDevice);

//	cudaWatch gpuWatch;

	dim3 block;
	dim3 grid;

	unsigned int uiCounter = 0u;
	
	
	// preparing sub warp pattern
	unsigned int hd = 0u;		// this bit pattern is used for warp scan for 2k-sub warps.

	unsigned int sift = 1u;	
	while(sift != 0u)
	{
		hd = hd | sift;
		sift =  sift <<(2*numOfNeighbors);
	}

#ifdef USING_FINAL_SORT
	sift = 1u;	
	unsigned int half_hd = 0u;	// this bit pattern is used for warp scan for k-sub warps.

	while(sift != 0u)
	{
		half_hd = half_hd | sift;
		sift =  sift << numOfNeighbors;
	}
#endif
	block.x = blocksize;
	grid.x = (sum_data_and_query+block.x-1)/block.x;

	#pragma unroll

	for(j=0.0f; j<= 0.25f; j+=0.05f)
	{
		
		initializeIndex<<<grid, block>>>( dDB_mixedindices_values.d_buffers[dDB_mixedindices_values.selector], sum_data_and_query);
		computeMortoncodes<<<grid, block>>>(d_x, d_y, d_z, dDB_mortoncodes_keys.d_buffers[dDB_mortoncodes_keys.selector], j, numDataPoints, numQueryPoints);

		if(uiCounter == 0u)
		{
			CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage_for_sort, temp_storage_bytes_for_sort, dDB_mortoncodes_keys, dDB_mixedindices_values, sum_data_and_query));
			CubDebugExit(cudaMalloc(&d_temp_storage_for_sort, temp_storage_bytes_for_sort));
		}


		CubDebugExit(cub::DeviceRadixSort::SortPairs(d_temp_storage_for_sort, temp_storage_bytes_for_sort, dDB_mortoncodes_keys, dDB_mixedindices_values, sum_data_and_query));
	
		// -- 1st sort done. 

		flagDataPoints<<<grid, block>>>(dDB_mixedindices_values.d_buffers[dDB_mixedindices_values.selector] , d_flags_for_data_points, numDataPoints, sum_data_and_query);
	

		// Run exclusive prefix min-scan
		if(uiCounter == 0u)
		{
			CubDebugExit(cub::DeviceScan::ExclusiveSum(d_temp_storage_for_es, temp_storage_bytes_for_es, d_flags_for_data_points, dDB_exclusivescanned_for_data_points.d_buffers[dDB_exclusivescanned_for_data_points.selector] , sum_data_and_query));
			CubDebugExit(cub::DevicePartition::Flagged(d_temp_storage_for_partition, temp_storage_bytes_for_partition, dDB_mixedindices_values.d_buffers[dDB_mixedindices_values.selector], d_flags_for_data_points, ptr_part_mixed_indices, d_num_selected_out, sum_data_and_query));
			CubDebugExit(cub::DevicePartition::Flagged(d_temp_storage_for_partition3, temp_storage_bytes_for_partition3, dDB_exclusivescanned_for_data_points.d_buffers[dDB_exclusivescanned_for_data_points.selector], d_flags_for_data_points, ptr_part_ex_sum, d_num_selected_out, sum_data_and_query));
			CubDebugExit(cub::DevicePartition::Flagged(d_temp_storage_for_partition4, temp_storage_bytes_for_partition4, dDB_mortoncodes_keys.d_buffers[dDB_mortoncodes_keys.selector], d_flags_for_data_points, ptr_part_mixed_mc, d_num_selected_out, sum_data_and_query));

			CubDebugExit(cudaMalloc(&d_temp_storage_for_es, temp_storage_bytes_for_es));		
			CubDebugExit(cudaMalloc(&d_temp_storage_for_partition, temp_storage_bytes_for_partition));
			CubDebugExit(cudaMalloc(&d_temp_storage_for_partition3, temp_storage_bytes_for_partition3));
			CubDebugExit(cudaMalloc(&d_temp_storage_for_partition4, temp_storage_bytes_for_partition4));
		}

		CubDebugExit(cub::DeviceScan::ExclusiveSum(d_temp_storage_for_es, temp_storage_bytes_for_es, d_flags_for_data_points, dDB_exclusivescanned_for_data_points.d_buffers[dDB_exclusivescanned_for_data_points.selector] , sum_data_and_query));
		CubDebugExit(cub::DevicePartition::Flagged(d_temp_storage_for_partition, temp_storage_bytes_for_partition, dDB_mixedindices_values.d_buffers[dDB_mixedindices_values.selector], d_flags_for_data_points, ptr_part_mixed_indices, d_num_selected_out, sum_data_and_query));
		CubDebugExit(cub::DevicePartition::Flagged(d_temp_storage_for_partition3, temp_storage_bytes_for_partition3, dDB_exclusivescanned_for_data_points.d_buffers[dDB_exclusivescanned_for_data_points.selector], d_flags_for_data_points, ptr_part_ex_sum, d_num_selected_out, sum_data_and_query));
		CubDebugExit(cub::DevicePartition::Flagged(d_temp_storage_for_partition4, temp_storage_bytes_for_partition4, dDB_mortoncodes_keys.d_buffers[dDB_mortoncodes_keys.selector], d_flags_for_data_points, ptr_part_mixed_mc, d_num_selected_out, sum_data_and_query));


		grid.x = (numQueryPoints+block.x-1)/block.x;
		
		storeNNCandidatesEuclidean<<<grid, block>>>(ptr_part_mixed_indices,  ptr_part_ex_sum,  ptr_part_mixed_mc, d_2kNNIndices, d_2kNNEuclideanDistances, d_2kNN_locally_sorted_mortoncodes,d_x, d_y,d_z, numDataPoints, numQueryPoints, numOfNeighbors);

		gpuErrchk( cudaPeekAtLastError() );
		gpuErrchk( cudaDeviceSynchronize() );
	// -- compact for query points


		grid.x = (2*numQueryPoints*numOfNeighbors+block.x-1)/block.x;

		if(uiCounter == 0u)
		{
			sortBasedOnEuclideanDistance<<<grid, block, (sizeof(unsigned int) + sizeof(float) )*blocksize>>>(d_AllNNIndices, d_AllNNEuclideanDistances, d_2kNNIndices, hd,d_x, d_y,d_z, numDataPoints, numQueryPoints, numOfNeighbors);

		
		}else
		{
			unsigned int numSharedArray = 8u;
			keep_K_NearestNeighborsEuclideanDist<<<grid,block, numSharedArray*sizeof(unsigned int)*blocksize>>>(d_AllNNIndices, d_AllNNEuclideanDistances, d_2kNNIndices, d_2kNNEuclideanDistances, numDataPoints, numQueryPoints, numOfNeighbors);
		}

		uiCounter++;

		printf("\n");
		gpuErrchk( cudaPeekAtLastError() );
		gpuErrchk( cudaDeviceSynchronize() );

	
	}


	cudaMemcpy(outIndices, d_AllNNIndices, sizeof(unsigned int) * numQueryPoints*numOfNeighbors, cudaMemcpyDeviceToHost);
	cudaMemcpy(outDistance, d_AllNNEuclideanDistances, sizeof(float) * numQueryPoints*numOfNeighbors, cudaMemcpyDeviceToHost);
	

	cudaFree(d_flags_for_data_points);
	cudaFree(d_data_point_indices);
	cudaFree(d_2kNNIndices);
	cudaFree(d_2kNNDistances);
	cudaFree(d_2kNNEuclideanDistances);
	cudaFree(d_AllNNEuclideanDistances);
	cudaFree(d_AllNNDistances);
	cudaFree(d_AllNNIndices);

	cudaFree(d_morton_keys_buf);
	cudaFree(d_morton_keys_alt_buf);
	cudaFree(d_mixed_indices_buf);
	cudaFree(d_mixed_indices_alt_buf);
	cudaFree(d_exclusivescanned_data_buf);
	cudaFree(d_exclusivescanned_data_alt_buf);
	cudaFree(ptr_part_mixed_indices);
	cudaFree(ptr_part_ex_sum);
	cudaFree(ptr_part_mixed_mc);

	cudaFree(d_x);
	cudaFree(d_y);
	cudaFree(d_z);
	cudaFree(d_num_selected_out);
	cudaFree(d_2kNN_locally_sorted_mortoncodes);
	
	cudaFree(d_temp_storage_for_sort);
	cudaFree(d_temp_storage_for_es);
	cudaFree(d_temp_storage_for_partition);
	cudaFree(d_temp_storage_for_partition3);
	cudaFree(d_temp_storage_for_partition4);

	delete[] px;
	delete[] py;
	delete[] pz;

}

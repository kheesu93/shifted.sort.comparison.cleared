#ifndef ___COME_ON_TJ____
#define ___COME_ON_TJ____

#pragma warning(disable: 4819)



#define SAFEDELETE(x)		{if(x != NULL) delete x; x = NULL;}
#define SAFEDELETEARRAY(x)		{if(x != NULL) delete[] x; x = NULL;}
#define SAFEFREE(x)		{if(x != NULL) free(x); x = NULL;}

#define SAFECUDADELETE(x) {if(x != NULL) cudaFree(x); x = NULL;}
#define	XEPS	0.01
#define cumax(a,b)	(a > b ? a: b)
#define cumin(a,b)	(a < b ? a: b)

//#define CUDA_DETERMINE_SIGN
//#define CUDAPOLYMORPHISM

/*

__device__ float3 operator+(const float3 &a, const float3 &b);
__device__ float3 operator-(const float3 &a, const float3 &b);
__device__ float3 operator*(const float3 &a, const float &b);
*/

#endif